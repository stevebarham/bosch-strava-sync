package dev.ethx.ebikeconnect

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jsonMapper
import com.fasterxml.jackson.module.kotlin.kotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.common.base.Preconditions
import com.google.common.base.Preconditions.checkNotNull
import net.ethx.shuteye.HttpTemplate
import net.ethx.shuteye.http.ContentType
import net.ethx.shuteye.http.except.ResponseException
import net.ethx.shuteye.http.request.Request
import net.ethx.shuteye.http.response.Response
import net.ethx.shuteye.http.response.trans.DefaultTransformer
import org.slf4j.LoggerFactory
import java.io.File
import java.io.InputStream
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*

object Main {
    val log = LoggerFactory.getLogger("bosch-strava-sync")

    @JvmStatic
    fun main(args: Array<String>) {
        val username = checkNotNull(System.getenv("USERNAME"), "No username defined")
        val password = checkNotNull(System.getenv("PASSWORD"), "No password defined")
        val statefile = File(checkNotNull(System.getenv("STATE"), "No state file defined"))
        val seen = if (statefile.exists()) statefile.readLines().map { it.toLong() }.toSortedSet() else TreeSet()

        log.info("Checking for new activities...")
        val svc = EbcApi.connect(username, password)
        for (activity in svc.completed()) {
            if (seen.add(activity.id)) {
                log.info("Found new completed activity ${activity.id}: ${activity.title} starting on ${activity.startDttm().toLocalDate()} at ${activity.startDttm().toLocalTime()} ending ${activity.duration().toMinutes()} minutes later at ${activity.endDttm().toLocalTime()}, checking if it can be exported to Strava...")
                if (svc.detail(activity).exportable()) {
                    svc.export(activity)
                    log.info("New activity ${activity.id} successfully exported to Strava")
                } else {
                    log.info("Skipping ${activity.id} as it has no coordinates")
                }
            }
        }

        statefile.writeText(seen.joinToString("\n"))
    }

    data class EbcLoginRequest(val username: String, val password: String, val rememberme: Boolean = false)
    data class EbcActivityHeader(
        @JsonProperty("id") val id: Long,
        @JsonProperty("title") val title: String?,
        @JsonProperty("ride_headers") val headers: List<EbcActivityHeader>?,
        @JsonProperty("status") val status: Int,
        @JsonProperty("start_time") val startTimeEpochMillis: Long,
        @JsonProperty("end_time") val endTimeEpochMillis: Long
    ) {
        fun startDttm() : LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(startTimeEpochMillis), ZoneId.systemDefault())
        fun endDttm() : LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(endTimeEpochMillis), ZoneId.systemDefault())
        fun duration() : Duration = Duration.between(startDttm(), endDttm())
    }

    data class EbcActivity(@JsonProperty("coordinates") val coords: List<List<List<Int?>>>) {
        fun exportable() : Boolean = coords.flatten().flatten().any { it != null }
    }

    class EbcApi private constructor(val cookie: String) {
        fun completed(max: Int = 100, until: Instant = Instant.now()) : List<EbcActivityHeader> = template
                .get("{+}/api/portal/activities/trip/headers{?max,offset}", root, max, until.toEpochMilli())
                .header("Accept", "application/json")
                .header("Protect-from", "CSRF")
                .header("Cookie", "JSESSIONID=$cookie")
                .map { _, stream -> mapper.readValue<List<EbcActivityHeader>>(stream) }
                .flatMap { it.headers ?: emptyList() }
                .filter { it.status == 1 }
                .sortedBy { it.id }

        fun export(activity: EbcActivityHeader) : String = template
            .post("{+}/api/portal/strava/upload/ride/${activity.id}?timezone=0", root)
            .header("Protect-from", "CSRF")
            .header("Cookie", "JSESSIONID=$cookie")
            .map { res, stream -> if (res.statusCode() >= 400) throw ResponseException(res) else String(stream.readBytes()) }

        fun detail(activity: EbcActivityHeader) : EbcActivity = template
            .get("{+}/api/activities/ride/details/${activity.id}", root)
            .header("Protect-from", "CSRF")
            .header("Cookie", "JSESSIONID=$cookie")
            .map { res, stream -> if (res.statusCode() >= 400) throw ResponseException(res) else {
                val string = stream.bufferedReader().readText()
                mapper.readValue(string)
            } }

        companion object {
            private val root = "https://www.ebike-connect.com/ebikeconnect"
            private val template = HttpTemplate()
            private val mapper = jsonMapper {
                addModule(kotlinModule())
                configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            }

            fun connect(username: String, password: String): EbcApi {
                return EbcApi(
                    template.post("{+}/api/portal/login/public", root)
                        .header("Accept", "application/json")
                        .body(EbcLoginRequest(username, password).toJson(), ContentType.create("application", "json"))
                        .map { response, _ ->
                            response.headers().first("Set-Cookie").substringAfter("JSESSIONID=").substringBefore(";").trim()
                        }
                )
            }

            fun <T> Request.map(f: (Response, InputStream) -> T) : T = this.`as`(object : DefaultTransformer<T>() {
                override fun handle(response: Response, stream: InputStream): T = f(response, stream)
            })

            fun Any.toJson(): String = mapper.writeValueAsString(this)
        }
    }
}